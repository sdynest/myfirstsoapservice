﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using MyFirstSoapService.DAL;
using MyFirstSoapService.Model;
using System.Data.Entity.Validation;
using System.Diagnostics;

namespace MyFirstSoapService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "AccidentService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select AccidentService.svc or AccidentService.svc.cs at the Solution Explorer and start debugging.
    public class AccidentService : IAccidentService
    {
        ServiceDbContext db = new ServiceDbContext();
        public Log Add(Accident newAccident)
        {
            try
            {
                db.Logger.Add(new LogTable { Type = 1, Date = DateTime.Now, Message = "Start Add method for Accident table: " + GetParametrs(newAccident), TargetSite = (new StackFrame()).GetMethod().ToString() });
                if ((newAccident.criminalName.Length > 3 && newAccident.criminalName.Length < 50) && (newAccident.dateOfAccident <= DateTime.Now) && (newAccident.valueOfRecovery >= 0))
                {
                    db.Accidents.Add(newAccident);
                    db.SaveChanges();
                    db.Logger.Add(new LogTable { Type = 2, Date = DateTime.Now, Message = "Successful : " + GetParametrs(newAccident), TargetSite = (new StackFrame()).GetMethod().ToString() });
                    return new Log("Success");
                }
                else
                {
                    db.Logger.Add(new LogTable { Type = 3, Date = DateTime.Now, Message = "Error during adding new entity : " + GetParametrs(newAccident), TargetSite = (new StackFrame()).GetMethod().ToString() });
                    return new Log("Error during adding new entity");
                }
            }
            catch
            {
                db.Logger.Add(new LogTable { Type = 3, Date = DateTime.Now, Message = "Error during adding new entity : " + GetParametrs(newAccident), TargetSite = (new StackFrame()).GetMethod().ToString() });
                return new Log("Invalid data in the fields. Name must be 3-50 symbols, Date must be less or equal to the current.");
            }
        }

        public Log Get(int id)
        {

            try
            {
                db.Logger.Add(new LogTable { Type = 1, Date = DateTime.Now, Message = "Start Get method for Accident table, getting entity with Id:  " + id.ToString(), TargetSite = (new StackFrame()).GetMethod().ToString() });
                Accident accidentForGet = db.Accidents.Where(Accident => Accident.id == id).FirstOrDefault();
                if (accidentForGet != null)
                {
                    db.Logger.Add(new LogTable { Type = 2, Date = DateTime.Now, Message = "Successful getting entity with Id:  " + id.ToString(), TargetSite = (new StackFrame()).GetMethod().ToString() });
                    return new Log(new List<Accident>() { accidentForGet }, "Success!");
                }
                else
                {
                    db.Logger.Add(new LogTable { Type = 3, Date = DateTime.Now, Message =  "Id:  " + id.ToString() + "not found", TargetSite = (new StackFrame()).GetMethod().ToString() });
                    return new Log("Id not found");
                }
            }
            catch
            {
                db.Logger.Add(new LogTable { Type = 3, Date = DateTime.Now, Message = "Id:  " + id.ToString() + "not found", TargetSite = (new StackFrame()).GetMethod().ToString() });
                return new Log("Critical Error");
            }

        }

        public Log GetAll()
        {
            try
            {
                db.Logger.Add(new LogTable { Type = 1, Date = DateTime.Now, Message = "Start GetAll method", TargetSite = (new StackFrame()).GetMethod().ToString() });
                db.Logger.Add(new LogTable { Type = 1, Date = DateTime.Now, Message = "Successful getting all entities", TargetSite = (new StackFrame()).GetMethod().ToString() });
                return new Log(db.Accidents.ToList(), "Success!");
            }
            catch
            {
                db.Logger.Add(new LogTable { Type = 3, Date = DateTime.Now, Message = "Error during executing GetAll method", TargetSite = (new StackFrame()).GetMethod().ToString() });
                return new Log("Error");
            }
        }

        public Log Update(Accident accident)
        {
            try
            {
                db.Logger.Add(new LogTable { Type = 1, Date = DateTime.Now, Message = "Start Add method for Accident table: " + GetParametrs(accident), TargetSite = (new StackFrame()).GetMethod().ToString() });
                if ((accident.criminalName.Length > 3 && accident.criminalName.Length < 50) && (accident.dateOfAccident <= DateTime.Now) && (accident.valueOfRecovery >= 0))
                {
                    db.Entry(accident).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();
                    db.Logger.Add(new LogTable { Type = 2, Date = DateTime.Now, Message = "Successful : " + GetParametrs(accident), TargetSite = (new StackFrame()).GetMethod().ToString() });
                    return new Log("Success");
                }
                else
                {
                    db.Logger.Add(new LogTable { Type = 3, Date = DateTime.Now, Message = "Error during updating new entity : " + GetParametrs(accident), TargetSite = (new StackFrame()).GetMethod().ToString() });
                    return new Log("Critical Error");
                }
            }
            catch
            {
                db.Logger.Add(new LogTable { Type = 3, Date = DateTime.Now, Message = "Invalid data in the fields. Name must be 3-50 symbols, Date must be less or equal to the current." + GetParametrs(accident), TargetSite = (new StackFrame()).GetMethod().ToString() });
                return new Log("Invalid data in the fields. Name must be 3-50 symbols, Date must be less or equal to the current.");
            }
        }

        public Log Remove(int id)
        {
            try
            {
                db.Logger.Add(new LogTable { Type = 1, Date = DateTime.Now, Message = "Start Remove method in Accident table for entity with id: " + id.ToString(), TargetSite = (new StackFrame()).GetMethod().ToString() });
                Accident accidentForRemove = db.Accidents.Find(id);
                if (null != accidentForRemove)
                {
                    db.Accidents.Remove(accidentForRemove);
                    db.SaveChanges();
                    db.Logger.Add(new LogTable { Type = 2, Date = DateTime.Now, Message = "Successful removing entity with id: " + id.ToString(), TargetSite = (new StackFrame()).GetMethod().ToString() });
                    return new Log("Successful removing entity with id: " + id.ToString());
                }
                else
                {
                    db.Logger.Add(new LogTable { Type = 3, Date = DateTime.Now, Message = "SError, the entity Id is not found, id entity: " + id.ToString(), TargetSite = (new StackFrame()).GetMethod().ToString() });
                    return new Log("Error");
                }
            }
            catch
            {
                db.Logger.Add(new LogTable { Type = 3, Date = DateTime.Now, Message = "Error, the entity Id is not found " + id.ToString(), TargetSite = (new StackFrame()).GetMethod().ToString() });
                return new Log("Error, the entity Id is not found");
            }
        }
        private string GetParametrs(Accident h)
        {
            string str = "";
            try
            {
                if (h != null)
                {
                    str += " Id=" + h.id;
                    str += " Name=" + h.criminalName;
                    str += " Date=" + h.dateOfAccident;
                    str += " Recovery=" + h.valueOfRecovery;
                    str += " Traffic Law=" + h.TrafficLaw;
                }
                return str;
            }
            catch (Exception ex)
            {
                db.Logger.Add(new LogTable() { Type = 3, Date = DateTime.Now, Message = ex.Message, Stack = ex.StackTrace, TargetSite = ex.TargetSite.ToString() });
                return "";
            }
        }
    }
}
