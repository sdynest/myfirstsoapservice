﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ServiceModel;
using System.Runtime.Serialization;

namespace MyFirstSoapService.Model
{
    [DataContract]
    public class Log
    {
        [DataMember]
        public List<TrafficLaw> TrafficLaw;
        [DataMember]
        public List<Heaviness> Heavinesses;
        [DataMember]
        public List<Accident> Accidents;
        [DataMember]
        public string Res;

        public Log(string _Error)
        {
            Res = _Error;
        }

        public Log(List<TrafficLaw> _TrafficLaw, string _Error)
        {
            TrafficLaw = _TrafficLaw;
            Res = _Error;
        }
        public Log(List<Heaviness> _Heavinesses, string _Error)
        {
            Heavinesses = _Heavinesses;
            Res = _Error;
        }
        public Log(List<Accident> _Accidents, string _Error)
        {
            Accidents = _Accidents;
            Res = _Error;
        }
    }
}