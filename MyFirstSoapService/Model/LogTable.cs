﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MyFirstSoapService.Model
{
    [DataContract]
    public class LogTable
    {
        [DataMember]
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [DataMember]
        [Required]
        [Range(1, 3)]
        public int Type { get; set; }

        [DataMember]
        [Required]
        [Column(TypeName = "ntext")]
        public string Message { get; set; }

        [DataMember]
        [Required]
        public DateTime Date { get; set; }

        [DataMember]
        [Required]
        [Column(TypeName = "ntext")]
        public string TargetSite { get; set; }

        [DataMember]
        public string Stack { get; set; }

        public override string ToString()
        {
            string _type = "";
            switch (this.Type)
            {
                case 1: _type = "DEBUG"; break;
                case 2: _type = "INFORMATION"; break;
                case 3: _type = "ERROR"; break;
            }

            return "Date: " + this.Date.ToString() + " Type: " + _type + " Method: " + this.TargetSite + " Stack: " + this.Stack + " Message: " + this.Message;
        }
    }
}