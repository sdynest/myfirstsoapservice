﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MyFirstSoapService.Model
{
    [DataContract(IsReference = true)]
    public class TrafficLaw
    {
        [DataMember]
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }

        [DataMember]
        [Column(TypeName = "VARCHAR")]
        [StringLength(50)]
        [Index("Rule title", IsUnique = true)]
        public string ruleTitle { get; set; }

        [DataMember]
        [Column(TypeName = "VARCHAR")]
        [StringLength(50)]
        [Index("Description")]
        public string description { get; set; }

        [DataMember]
        [Column(TypeName = "INT")]
        [Index("Recovery from")]
        public int recoveryFrom { get; set; }

        [DataMember]
        [Column(TypeName = "INT")]
        [Index("Recodvery to")]
        public int recoveryTo { get; set; }

        [DataMember]
        public Heaviness Heaviness { get; set; }

        [DataMember]
        public List<Accident> Accidents { get; set; }
    }
}