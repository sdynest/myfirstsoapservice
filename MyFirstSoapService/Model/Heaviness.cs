﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MyFirstSoapService.Model
{
    [DataContract(IsReference = true)]
    public class Heaviness
    {
        [DataMember]
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }

        [DataMember]
        [Column(TypeName = "VARCHAR")]
        [StringLength(50)]
        [Index("Heavy title", IsUnique = true)]
        public string heavyTitle { get; set; }

        [DataMember]
        [Column(TypeName = "VARCHAR")]
        [StringLength(50)]
        [Index("Description")]
        public string description { get; set; }

        [DataMember]
        public List<TrafficLaw> TrafficLaws { get; set; }
    }
}