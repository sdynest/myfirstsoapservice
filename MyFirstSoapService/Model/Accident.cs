﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MyFirstSoapService.Model
{ 
    [DataContract(IsReference = true)]
    public class Accident
    {
        [DataMember]
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }

        [DataMember]
        [Column(TypeName = "VARCHAR")]
        [StringLength(50)]
        [Index("Criminal name", IsUnique = true)]
        public string criminalName { get; set; }

        [DataMember]
        [Index("Time of accident")]
        [Column(TypeName = "date")]
        public DateTime dateOfAccident { get; set; }

        [DataMember]
        [Column(TypeName = "int")]
        [Index("Value of recovery")]
        public int valueOfRecovery { get; set; }

        [DataMember]
        public TrafficLaw TrafficLaw { get; set; }
    }
}