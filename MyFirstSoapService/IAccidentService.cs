﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using MyFirstSoapService.Model;

namespace MyFirstSoapService
{
    [ServiceContract]
    public interface IAccidentService
    {
        [OperationContract]
        Log Add(Accident newAccident);

        [OperationContract]
        Log Get(int id);

        [OperationContract]
        Log GetAll();

        [OperationContract]
        Log Update(Accident accident);

        [OperationContract]
        Log Remove(int id);
    }
}
