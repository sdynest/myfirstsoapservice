﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using MyFirstSoapService.Model;
using MyFirstSoapService.DAL;

namespace MyFirstSoapService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "LogTableService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select LogTableService.svc or LogTableService.svc.cs at the Solution Explorer and start debugging.
    public class LogTableService : ILogTableService
    {
        ServiceDbLogTableContext db = new ServiceDbLogTableContext();

        public List<string> GetAll(int type = 0, string date_time = null)
        {
            List<string> list = new List<string>();
            try
            {
                List<LogTable> logTables = new List<LogTable>();
                if (type != 0 && date_time != null)
                {
                    logTables = db.logsTable.Where(i => i.Type == type && i.Date == DateTime.Parse(date_time)).ToList();
                }
                else if (type != 0)
                {
                    logTables = db.logsTable.Where(i => i.Type == type).ToList();
                }
                else if (date_time != null)
                {
                    logTables = db.logsTable.Where(i => i.Type == type).ToList();
                }
                else
                {
                    logTables = db.logsTable.ToList();
                }
                foreach (var j in logTables)
                {
                    list.Add(j.ToString());
                }
            }
            catch (Exception ex)
            {
                LogTable l = new LogTable() { Type = 3, Date = DateTime.Now, Message = ex.Message, Stack = ex.StackTrace, TargetSite = ex.TargetSite.ToString() };
                list.Add(l.ToString());
                (new LogManager()).Add(l);
            }
            return list;
        }
    }
}
