﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using MyFirstSoapService.DAL;
using MyFirstSoapService.Model;
using System.Data.Entity.Validation;
using System.Diagnostics;

namespace MyFirstSoapService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "TrafficLawService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select TrafficLawService.svc or TrafficLawService.svc.cs at the Solution Explorer and start debugging.
    public class TrafficLawService : ITrafficLawService
    {
        ServiceDbContext db = new ServiceDbContext();
        public Log Add(TrafficLaw newTrafficLaw)
        {
            db.Logger.Add(new LogTable { Type = 1, Date = DateTime.Now, Message = "Start Add method for TrafficLaw table: " + GetParametrs(newTrafficLaw), TargetSite = (new StackFrame()).GetMethod().ToString() });
            try
            {
                if ((newTrafficLaw.ruleTitle.Length > 2 && newTrafficLaw.ruleTitle.Length < 50) && newTrafficLaw.description.Length > 2 && newTrafficLaw.description.Length <= 50 && newTrafficLaw.recoveryFrom > 0 && newTrafficLaw.recoveryTo > 0 && ((newTrafficLaw.recoveryTo - newTrafficLaw.recoveryFrom) > 0))
                {
                    db.TrafficLaws.Add(newTrafficLaw);
                    db.SaveChanges();
                    db.Logger.Add(new LogTable { Type = 2, Date = DateTime.Now, Message = "Successful adding new accident in TrafficLaw table: " + GetParametrs(newTrafficLaw), TargetSite = (new StackFrame()).GetMethod().ToString() });
                    return new Log("Success");
                }
                else
                {
                    db.Logger.Add(new LogTable { Type = 3, Date = DateTime.Now, Message = "Error: " + GetParametrs(newTrafficLaw), TargetSite = (new StackFrame()).GetMethod().ToString() });
                    return new Log("Error during validation!");
                }
            }
            catch
            {
                db.Logger.Add(new LogTable { Type = 3, Date = DateTime.Now, Message = "Error during adding. Title and Description must be 2-50 symbol, rerovery must be positive: " + GetParametrs(newTrafficLaw), TargetSite = (new StackFrame()).GetMethod().ToString() });
                return new Log("Title and Description must be 2-50 symbol, rerovery must be positive");
            }
        }

        public Log Get(int id)
        {
            try
            {
                db.Logger.Add(new LogTable { Type = 1, Date = DateTime.Now, Message = "Start Get method for TrafficLaw table, get Id: " + id.ToString(), TargetSite = (new StackFrame()).GetMethod().ToString() });
                TrafficLaw trafficLawForGet = db.TrafficLaws.Include("Accidents").Where(TrafficLaw => TrafficLaw.id == id).FirstOrDefault();
                if (trafficLawForGet != null)
                {
                    db.Logger.Add(new LogTable { Type = 2, Date = DateTime.Now, Message = "Success to get Id: " + id.ToString(), TargetSite = (new StackFrame()).GetMethod().ToString() });
                    return new Log(new List<TrafficLaw>() { trafficLawForGet }, "Success!");
                }
                else
                {
                    db.Logger.Add(new LogTable { Type = 3, Date = DateTime.Now, Message = "Error to get Id, Id: " + id.ToString() + " not found", TargetSite = (new StackFrame()).GetMethod().ToString() });
                    return new Log("Id not found");
                }
            }
            catch
            {
                db.Logger.Add(new LogTable { Type = 3, Date = DateTime.Now, Message = "Critical error to get Id: " + id.ToString(), TargetSite = (new StackFrame()).GetMethod().ToString() });
                return new Log("Critical Error");
            }
        }

        public Log GetAll()
        {
            try
            {
                db.Logger.Add(new LogTable { Type = 1, Date = DateTime.Now, Message = "Start GetAll method for TrafficLaw table", TargetSite = (new StackFrame()).GetMethod().ToString() });
                return new Log(db.TrafficLaws.Include("Accidents").ToList(), "Success!");
            }
            catch
            {
                db.Logger.Add(new LogTable { Type = 3, Date = DateTime.Now, Message = "Error during get all entities", TargetSite = (new StackFrame()).GetMethod().ToString() });
                return new Log("Error");
            }
        }

        public Log Update(TrafficLaw trafficLaw)
        {
            try
            {
                db.Logger.Add(new LogTable { Type = 1, Date = DateTime.Now, Message = "Start Update method for TrafficLaw table: " + GetParametrs(trafficLaw), TargetSite = (new StackFrame()).GetMethod().ToString() });
                if ((trafficLaw.ruleTitle.Length > 2 && trafficLaw.ruleTitle.Length < 50) && trafficLaw.description.Length > 2 && trafficLaw.description.Length <= 50 && trafficLaw.recoveryFrom > 0 && trafficLaw.recoveryTo > 0 && ((trafficLaw.recoveryTo - trafficLaw.recoveryFrom) > 0))
                {
                    db.Entry(trafficLaw).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();
                    db.Logger.Add(new LogTable { Type = 2, Date = DateTime.Now, Message = "Successful updating new accident in TrafficLaw table: " + GetParametrs(trafficLaw), TargetSite = (new StackFrame()).GetMethod().ToString() });
                    return new Log("Success");
                }
                else
                {
                    db.Logger.Add(new LogTable { Type = 3, Date = DateTime.Now, Message = "Error during update entity: " + GetParametrs(trafficLaw), TargetSite = (new StackFrame()).GetMethod().ToString() });
                    return new Log("Error during validation!");
                }
            }
            catch
            {
                db.Logger.Add(new LogTable { Type = 3, Date = DateTime.Now, Message = "Error during update entity: " + GetParametrs(trafficLaw), TargetSite = (new StackFrame()).GetMethod().ToString() });
                return new Log("Title and Description must be 2-50 symbol, rerovery must be positive");
            }
        }

        public Log Remove(int id)
        {
            try
            {
                db.Logger.Add(new LogTable { Type = 1, Date = DateTime.Now, Message = "Start Remove method for TrafficLaw table: " + id.ToString(), TargetSite = (new StackFrame()).GetMethod().ToString() });
                TrafficLaw trafficLawForRemove = db.TrafficLaws.Find(id);
                if (null != trafficLawForRemove)
                {
                    db.TrafficLaws.Remove(trafficLawForRemove);
                    db.SaveChanges();
                    db.Logger.Add(new LogTable { Type = 2, Date = DateTime.Now, Message = "Success during remove entity: " + id.ToString(), TargetSite = (new StackFrame()).GetMethod().ToString() });
                    return new Log("Success");
                }
                else
                {
                    db.Logger.Add(new LogTable { Type = 3, Date = DateTime.Now, Message = "Critical error during remove entity with id: " + id.ToString(), TargetSite = (new StackFrame()).GetMethod().ToString() });
                    return new Log("Error!");
                }
            }
            catch
            {
                db.Logger.Add(new LogTable { Type = 3, Date = DateTime.Now, Message = "Error. Entity not found, during remove entity with id: " + id.ToString(), TargetSite = (new StackFrame()).GetMethod().ToString() });
                return new Log("Error, the entity Id is not found");
            }
        }
        private string GetParametrs(TrafficLaw t)
        {
            string str = "";
            try
            {
                if (t != null)
                {
                    str += " Id=" + t.id;
                    str += " Name=" + t.ruleTitle;
                    str += " Description=" + t.description;
                    str += " Recovery from=" + t.recoveryFrom;
                    str += " Recovery to=" + t.recoveryTo;
                    str += " Accidents={";
                    if (t.Accidents != null)
                    {
                        foreach (var i in t.Accidents)
                        {
                            str += "Id=" + i.id + ",";
                        }
                    }
                    str += "}";
                }
                return str;
            }
            catch (Exception ex)
            {
                db.Logger.Add(new LogTable() { Type = 3, Date = DateTime.Now, Message = ex.Message, Stack = ex.StackTrace, TargetSite = ex.TargetSite.ToString() });
                return "";
            }

        }
    }
}
