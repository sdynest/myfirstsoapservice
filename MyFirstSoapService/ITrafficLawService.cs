﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using MyFirstSoapService.Model;

namespace MyFirstSoapService
{
    [ServiceContract]
    public interface ITrafficLawService
    {
        [OperationContract]
        Log Add(TrafficLaw newTrafficLaw);

        [OperationContract]
        Log Get(int id);

        [OperationContract]
        Log GetAll();

        [OperationContract]
        Log Update(TrafficLaw TrafficLaw);

        [OperationContract]
        Log Remove(int id);
    }
}
