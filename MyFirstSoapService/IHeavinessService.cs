﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using MyFirstSoapService.Model;

namespace MyFirstSoapService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IHeavinessService" in both code and config file together.
    [ServiceContract]
    public interface IHeavinessService
    {
        [OperationContract]
        Log Add(Heaviness newHeaviness);

        [OperationContract]
        Log Get(int id);

        [OperationContract]
        Log GetAll();

        [OperationContract]
        Log Update(Heaviness Heaviness);

        [OperationContract]
        Log Remove(int id);
    }
}
