﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using MyFirstSoapService.Model;
using System.Diagnostics;

namespace MyFirstSoapService.DAL
{ 
    public class ServiceDbLogTableContext: DbContext
    {
        public ServiceDbLogTableContext()
            : base("ServiceDbLogContext")
        {
            Configuration.AutoDetectChangesEnabled = true;
            Configuration.LazyLoadingEnabled = true;
            Configuration.ProxyCreationEnabled = true;
            Configuration.ValidateOnSaveEnabled = true;
        }

        public DbSet<LogTable> logsTable { get; set; }
    }

    public class ServiceDbLogInitializer : DropCreateDatabaseIfModelChanges<ServiceDbLogTableContext>
    {
        protected override void Seed(ServiceDbLogTableContext context)
        {
            context.logsTable.Add(new LogTable() { Type = 2, Date = DateTime.Now, Message = "Logging database was create.", TargetSite = (new StackFrame()).GetMethod().ToString() });
            context.SaveChanges();
        }
    }
}