﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MyFirstSoapService.Model;
using System.Data.Entity.Validation;
using System.Data.Entity;
using System.Diagnostics;

namespace MyFirstSoapService.DAL
{
    public class LogManager
    {
        public ServiceDbLogTableContext db = new ServiceDbLogTableContext();

        public bool Add(LogTable newLog)
        {
            try
            {
                db.logsTable.Add(newLog);
                db.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }
    }



    public class ServiceDbContext : DbContext
    {
        public ServiceDbContext()
           : base("ServiceDbContext")
        {
            Configuration.AutoDetectChangesEnabled = true;
            Configuration.LazyLoadingEnabled = true;
            Configuration.ProxyCreationEnabled = true;
            Configuration.ValidateOnSaveEnabled = true;
        }
        public DbSet<Heaviness> Heavinesses { get; set; }
        public DbSet<TrafficLaw> TrafficLaws { get; set; }
        public DbSet<Accident> Accidents { get; set; }

        public LogManager Logger = new LogManager();

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //Описание "Тяжесть" -< "Статьи" -< "Преступления"
            modelBuilder.Entity<Heaviness>()
                        .HasMany(s => s.TrafficLaws) // у тяжести много статей
                        .WithRequired(s => s.Heaviness); // у статьи одна тяжесть

            modelBuilder.Entity<TrafficLaw>()
                        .HasMany(s => s.Accidents)// у статей много преступлений
                        .WithRequired(s => s.TrafficLaw); // у дпреступления одна статья

            base.OnModelCreating(modelBuilder);

        }

        protected override void Dispose(bool disposing)
        {
            Configuration.LazyLoadingEnabled = false;
            base.Dispose(disposing);
        }
    }
    public class ServiceDbContextInitializer : DropCreateDatabaseIfModelChanges<ServiceDbContext>
    {
        protected override void Seed(ServiceDbContext context)
        {
            context.Logger.Add(new LogTable { Type = 1, Date = DateTime.Now, Message = "Initializing main database...", TargetSite = (new StackFrame()).GetMethod().ToString() });
            try
            {
                Heaviness heaviness_1 = context.Heavinesses.Add(new Heaviness() { heavyTitle = "Some title", description = "Some description" });
                TrafficLaw trafficLaw_1 = context.TrafficLaws.Add(new TrafficLaw() { ruleTitle = "Some rule title", description = "Some rule description", recoveryFrom = 100, recoveryTo = 500, Heaviness = heaviness_1 });
                Accident accident_1 = context.Accidents.Add(new Accident() { criminalName = "SomeName", dateOfAccident = DateTime.Now, valueOfRecovery = 400, TrafficLaw = trafficLaw_1 });
                context.SaveChanges();
                context.Logger.Add(new LogTable { Type = 1, Date = DateTime.Now, Message = "Initialize the main database is successful.", TargetSite = (new StackFrame()).GetMethod().ToString() });
            }
            catch (Exception ex)
            {
                context.Logger.Add(new LogTable() { Type = 3, Date = DateTime.Now, Message = ex.Message, Stack = ex.StackTrace, TargetSite = ex.TargetSite.ToString() });
            }
        }
    }
}