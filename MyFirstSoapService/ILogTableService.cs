﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace MyFirstSoapService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "ILogTableService" in both code and config file together.
    [ServiceContract]
    public interface ILogTableService
    {
        [OperationContract]
        List<string> GetAll(int type, string date_time);
    }
}
