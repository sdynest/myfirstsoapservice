﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using MyFirstSoapService.DAL;
using MyFirstSoapService.Model;
using System.Data.Entity.Validation;
using System.Diagnostics;

namespace MyFirstSoapService
{
    public class HeavinessService : IHeavinessService
    {
        ServiceDbContext db = new ServiceDbContext();
        public Log Add(Heaviness newHeaviness)
        {
            db.Logger.Add(new LogTable { Type = 1, Date = DateTime.Now, Message = "Start Add method for Heaviness table: " + GetParametrs(newHeaviness), TargetSite = (new StackFrame()).GetMethod().ToString() });
            try
            {
                if ((newHeaviness.heavyTitle.Length > 2 && newHeaviness.heavyTitle.Length < 50) && newHeaviness.description.Length > 2 && newHeaviness.description.Length <= 50)
                {
                    db.Heavinesses.Add(newHeaviness);
                    db.SaveChanges();
                    db.Logger.Add(new LogTable { Type = 2, Date = DateTime.Now, Message = "Successful adding " + GetParametrs(newHeaviness), TargetSite = (new StackFrame()).GetMethod().ToString() });
                    return new Log("Success");
                }
                else
                {
                    db.Logger.Add(new LogTable { Type = 3, Date = DateTime.Now, Message = "Error during validation! " + GetParametrs(newHeaviness), TargetSite = (new StackFrame()).GetMethod().ToString() });
                    return new Log("Error during validation!");
                }
            }
            catch
            {
                db.Logger.Add(new LogTable { Type = 2, Date = DateTime.Now, Message = "Title and Description must be 2-50 symbols " + GetParametrs(newHeaviness), TargetSite = (new StackFrame()).GetMethod().ToString() });
                return new Log("Title and Description must be 2-50 symbols");
            }
        }

        public Log Get(int id)
        {
            try
            {
                db.Logger.Add(new LogTable { Type = 1, Date = DateTime.Now, Message = "Start Get method for Heaviness table: " + id.ToString(), TargetSite = (new StackFrame()).GetMethod().ToString() });
                Heaviness heavinessForGet = db.Heavinesses.Include("TrafficLaws").Include("TrafficLaws.Accidents").Where(Heaviness => Heaviness.id == id).FirstOrDefault();
                if (heavinessForGet != null)
                {
                    return new Log(new List<Heaviness>() { heavinessForGet }, "Success!");
                }
                else
                {
                    db.Logger.Add(new LogTable { Type = 2, Date = DateTime.Now, Message = "Id not found in Heaviness table " + id.ToString(), TargetSite = (new StackFrame()).GetMethod().ToString() });
                    return new Log("Id not found");
                }
            }
            catch
            {
                db.Logger.Add(new LogTable { Type = 3, Date = DateTime.Now, Message = "Error during Get method execution.  " + id.ToString(), TargetSite = (new StackFrame()).GetMethod().ToString() });
                return new Log("Error, maybe id field was empty");
            }
        }

        public Log GetAll()
        {
            try
            {
                db.Logger.Add(new LogTable { Type = 1, Date = DateTime.Now, Message = "Start GetAll method for Heaviness table: ", TargetSite = (new StackFrame()).GetMethod().ToString() });
                return new Log(db.Heavinesses.Include("TrafficLaws").Include("TrafficLaws.Accidents").ToList(), "Success!");
            }
            catch
            {
                db.Logger.Add(new LogTable { Type = 3, Date = DateTime.Now, Message = "Errpr during GetAll method execution ", TargetSite = (new StackFrame()).GetMethod().ToString() });
                return new Log("Error");
            }
        }

        public Log Update(Heaviness heaviness)
        {
            try
            {
                db.Logger.Add(new LogTable { Type = 1, Date = DateTime.Now, Message = "Start Update method for Heaviness table: " + GetParametrs(heaviness), TargetSite = (new StackFrame()).GetMethod().ToString() });
                if ((heaviness.heavyTitle.Length > 2 && heaviness.heavyTitle.Length < 50) && heaviness.description.Length > 2 && heaviness.description.Length <= 50)
                {
                    db.Entry(heaviness).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();
                    db.Logger.Add(new LogTable { Type = 2, Date = DateTime.Now, Message = "Successful updating " + GetParametrs(heaviness), TargetSite = (new StackFrame()).GetMethod().ToString() });
                    return new Log("Success");
                }
                else
                {
                    db.Logger.Add(new LogTable { Type = 3, Date = DateTime.Now, Message = "Error during validation! " + GetParametrs(heaviness), TargetSite = (new StackFrame()).GetMethod().ToString() });
                    return new Log("Error during validation!");
                }
            }
            catch
            {
                db.Logger.Add(new LogTable { Type = 2, Date = DateTime.Now, Message = "Title and Description must be 2-50 symbols " + GetParametrs(heaviness), TargetSite = (new StackFrame()).GetMethod().ToString() });
                return new Log("Title and Description must be 2-50 symbols");
            }
        }

        public Log Remove(int id)
        {
            try
            {
                db.Logger.Add(new LogTable { Type = 1, Date = DateTime.Now, Message = "Start Remove method for Heaviness table: " + id.ToString(), TargetSite = (new StackFrame()).GetMethod().ToString() });
                Heaviness heavinessForRemove = db.Heavinesses.Find(id);
                if (null != heavinessForRemove && id > 0)
                {
                    db.Heavinesses.Remove(heavinessForRemove);
                    db.SaveChanges();
                    db.Logger.Add(new LogTable { Type = 2, Date = DateTime.Now, Message = "Successful remove : " + id.ToString(), TargetSite = (new StackFrame()).GetMethod().ToString() });
                    return new Log("Success");
                }
                else
                {
                    db.Logger.Add(new LogTable { Type = 3, Date = DateTime.Now, Message = "Error, the entity Id is not found : " + id.ToString(), TargetSite = (new StackFrame()).GetMethod().ToString() });
                    return new Log("Error, the entity Id is not found");
                }
            }
            catch
            {
                db.Logger.Add(new LogTable { Type = 3, Date = DateTime.Now, Message = "Error, the entity Id is not found : " + id.ToString(), TargetSite = (new StackFrame()).GetMethod().ToString() });
                return new Log("Error, the entity Id is not found");
            }

        }
        private string GetParametrs(Heaviness h)
        {
            string str = "";
            try
            {
                if (h != null)
                {
                    str += " Id=" + h.id;
                    str += " Name=" + h.heavyTitle;
                    str += " Code=" + h.description;
                    str += " Purchases={";
                    if (h.TrafficLaws != null)
                    {
                        foreach (var t in h.TrafficLaws)
                        {
                            str += "Id=" + t.id + ",";
                        }
                    }
                    str += "}";
                }
                return str;
            }
            catch (Exception ex)
            {
                db.Logger.Add(new LogTable() { Type = 3, Date = DateTime.Now, Message = ex.Message, Stack = ex.StackTrace, TargetSite = ex.TargetSite.ToString() });
                return "";
            }

        }
    }
}
